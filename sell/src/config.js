/**
 * Created by Luby on 2018/5/29.
 */
// 配置项

import { VeevlinkOssUrl } from './utils/base'
//   测试环境的变量
const host = 'http://localhost:8082';

if(VeevlinkOssUrl() === 'https://ebensz-sales.oss-cn-hangzhou.aliyuncs.com'){
  console.log('是正式图片');
  const url = 'https://pic.8848phone.com/custom/8848/image/';
}else{
  const url = 'https://oss-test.veevlink.com/custom/8848/image/';
}
const url = 'https://tests.veevlink.com/custom/redstone/img/';
const href = function (url,redirectTo) {
  console.log(url);
  if(redirectTo){
    wx.redirectTo({url : url})
  }else{
    wx.navigateTo({url : url})
  }
};
const back =  function () {
  wx.navigateBack();
};
// const back =  function (url) {
//   if(url){
//     wx.reLaunch({url : url})
//   }else{
//     wx.navigateBack();
//   }
// };


const config ={
  url,
  host,
  href,
  back,
  loginUrl:`${host}/weapp/login`,
  /*
 主包页面
  */
  mainPage:{
    confirm:'/pages/mainPage/confirm/main',// 授权登录
    login:'/pages/mainPage/login/main',// 登录
    home:'/pages/mainPage/home/main',// 主页
    register:'/pages/mainPage/register/main',//登录/注册
    agree:'/pages/mainPage/agree/main',// 用户许可协议
    info:'/pages/mainPage/info/main',// 我的信息
    myright:'/pages/mainPage/myright/main',// 我的权益
    recordrule:'/pages/mainPage/recordrule/main',// 积分规则
    notify:'/pages/mainPage/notify/main',// 消息通知
    first:'/pages/mainPage/first/main',// 加载
    addinfo:'/pages/mainPage/addinfo/main'// 完善信息
  },
  /*积分*/
  //积分商城
  shop:{
    home:'/pages/shop/home/main',//积分商城主页
    detail:'/pages/shop/detail/main',//详情页
    record:'/pages/shop/record/main',//兑换记录
    logview:'/pages/shop/logview/main',//查看物流
    search:'/pages/shop/search/main',//查找商品
    confirm:'/pages/shop/confirm/main',//确认兑换
    recordinfo:'/pages/shop/recordinfo/main',//积分明细
    bannerWebView: '/pages/shop/banner-web-view/main', //banner外链
  },
  //我的卡券
  mycard:{
    card: '/pages/mycard/card/main',//我的卡券
    cardinfo: '/pages/mycard/cardinfo/main',//卡券详情
    store: '/pages/mycard/store/main',//适用门店
    type: '/pages/mycard/type/main',//适用类型
    detail: '/pages/mycard/detail/main',//券详情
    recieve: '/pages/mycard/recieve/main',//卡券领取
  },
  //我的活动
  activity:{
    detail: '/pages/activity/detail/main',//活动详情
    list: '/pages/activity/list/main',//活动列表
  },
   //我的地址
   address:{
    addlist: '/pages/address/addlist/main',//新建/编辑地址
    add: '/pages/address/add/main',//新建/编辑地址
  },
  //专属民牌顾问
  customer :{
    home: '/pages/customer/home/main',//专属品牌顾问
    add: '/pages/customer/add/main',//试衣预约时间选择
    record: '/pages/customer/record/main',//试衣记录
    detail: '/pages/customer/detail/main',//详情
    order: '/pages/customer/order/main',//试衣预约
    reason: '/pages/customer/reason/main',//取消原因
    service: '/pages/customer/service/main',//售后服务
    store: '/pages/customer/store/main',//门店选择
    list: '/pages/customer/list/main'//门店选择
  },
   //其他链接
   other :{
    add: '/pages/other/changerecord/main',//改衣记录
    searchstore: '/pages/other/searchstore/main',//查找店铺
    detail: '/pages/other/detail/main',//详情
    suggestion: '/pages/other/suggestion/main',//保养建议
    integraldetails: '/pages/other/integraldetails/main',//积分明细

  },

};



export default config;
