/**
 * Created by Luby on 2018/7/30.
 */
//判断运行环境true
const formal = false;  // 是否是正式环境

let context;
if(formal){
  // 正式环境
  context = {
    // url : 'https://ap4ycdn.veevlink.com',
    url : 'https://ap1.veevlink.com',
    appid :'wx23bcedfe9e867c13',
    ossurl : 'https://ebensz-sales.oss-cn-hangzhou.aliyuncs.com',
    ossshowurl : 'https://pic.8848phone.com',
    iv : '',
    data : ''
  };
}else{
  //测试环境
  context = {
    url : 'https://test.veevlink.com',
    appid :'wx23bcedfe9e867c13',
    ossurl : 'https://veevlink-test.oss-cn-qingdao.aliyuncs.com',
    ossshowurl : 'https://oss-test.veevlink.com',
    iv : '',
    data : ''
  };
}


export function VeevlinkIslogin(success,error,close) {
  let num = 0;
  console.log('1');
  function login() {
    console.log('login',wx.getStorageSync('openid'+context.appid));
    // 判断有没有用户信息有的话不掉接口
    wx.login({
      success: function (res) {
        if(wx.getStorageSync('openid'+context.appid)){
          // success(wx.getStorageSync('openid'+context.appid));
          wx.request({
            url: context.url+'/VeevlinkApplication/AppUserQuery.aspx',
            method:'POST',
            data: {
              appid: context.appid,
              openId: wx.getStorageSync('openid'+context.appid)
            },
            header: {
              'Content-Type': 'application-json'

            },
            success: function (res) {
              console.log('resdata',res.data);
              wx.setStorageSync('information'+context.appid, res.data);
              success(wx.getStorageSync('openid'+context.appid));
            },
            fail:function (err) {
              error(context.appid)
            }
          });
        }else{
          wx.request({
            url: context.url+'/VeevlinkApplication/AppOpenIdQuery.aspx',
            data: {
              appid:context.appid,
              code: res.code
            },
            header: {
              'Content-Type':'application-json'
            },
            success:function(res2){
              console.log('3');
              console.log(res2);
              if(!res2.data){
                close('openid获取失败');
                wx.hideLoading();
                return;
              }
              wx.setStorageSync('openid'+context.appid, res2.data);
              login();
            },
            fail:function (err) {
              wx.hideLoading();
              close('网络出错!请稍后重试')
            }
          })
        }
      },
      fail: function (res) {
        // console.log('客户拒绝授权');
        // login();
        wx.hideLoading();
        close('授权失败!请退出重试')
      },
    });

  }
  login();
}



export function VeevlinkLogin(data,iv,type, success,error) {
  console.log('iv',iv);
  console.log('data',data);
  context.iv = iv;
  context.data = data;
  // 用户登录获取授权信息

  //判断过期缓存
  // let nowtime = Date.parse(new Date());
  // if( wx.getStorageSync('timestamp'+context.appid) ){
  //   if(nowtime -  parseInt(wx.getStorageSync('timestamp'+context.appid)) > 7200000 ){
  //
  //   }else{
  //     wx.hideLoading();
  //     success();
  //     return;
  //   }
  // }
  // 登录
  wx.showLoading({
    title: '加载中',
  });
  wx.login({
    success: function (res) {
      console.log('VeevlinkLogin',res);
      console.log("code:"+res.code);
      wx.setStorageSync('code', res.code);
      console.log('12313');
      let Code = res.code;
      // 发送 res.code 到后台换取 openId, sessionKey, unionId
      if(res){
        wx.request({
          url: context.url+'/VeevlinkApplication/AppOpenIdQuery.aspx',
          data: {
            appid:context.appid,
            code: res.code
          },
          header: {
            'Content-Type':'application-json'
          },
          success:function(res){
            console.log('res',res);
            console.log("openid==="+res.data)
            let OpenId = res.data;
            // 存openid
            wx.setStorageSync('openid'+context.appid, res.data);
            if(type === 'phone'){
              console.log('phone');
              wx.checkSession({
                success (data) {
                  //session_key 未过期，并且在本生命周期一直有效
                  console.log("su",data)
                  wx.request({
                    url: context.url+'/VeevlinkApplication/AppTelphone.aspx',
                    // url:'http://dev.veevlink.com/veevlinkapplication/AppTelphone.aspx',
                    //method:'POST',
                    data: {
                      appid: context.appid,
                      openId: OpenId,
                      //code: Code,
                      encryptedData: context.data,
                      vi: context.iv
                    },
                    header: {
                      'Content-Type': 'application-json'
          
                    },
                    success: function (res) {
                      console.log('获取信息phone',res.data);
                      // wxe3786e0f1c17f2f9
                      wx.setStorageSync('phone'+context.appid, res.data);
          
                      wx.hideLoading();
                      let timestamp = Date.parse(new Date());
                      wx.setStorageSync('timestamp'+context.appid, timestamp);
                      console.log('获取信息成功');
                      // wx.navigateTo({
                      //   url: '/pages/mainPage/login/main?id=1',
                      //   success: function(res) {
                      //     // 通过eventChannel向被打开页面传送数据
                      //     console.log('跳转页面成功',res);
                      //   },
                      //   fail:function(res){
                      //     console.log('跳转页面失败',res);
                      //   }
                      // })
                      // // 本地存储openId,时效为2小时
                      // wx.setStorageSync('openId', res.data)
                      success();
                    }});
                },
                fail (data) {
                  // session_key 已经失效，需要重新执行登录流程
                  console.log("dai",data)
                  VeevlinkLogin(context.data,context.iv);
                  //wx.login() //重新登录
                }
              })

            }
            if(type==='confirm'){
              console.log('confirm');
              // 获取用户信息
            wx.getSetting({
              success: res => {
                console.log('getSetting',res);
                if (res.authSetting['scope.userInfo']) {
                  // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
                  wx.getUserInfo({
                    lang:'zh_CN',
                    success: function(res) {
                      console.log(res);
                      let UserInfo = res.encryptedData //拿的是加密的
                      let iv = res.iv
                      wx.request({
                        url: context.url+'/VeevlinkApplication/AppUserQuery.aspx',
                        method:'POST',
                        data: {
                          appid: context.appid,
                          openId: OpenId,
                          code: Code,
                          userInfo: UserInfo,
                          iv: iv
                        },
                        header: {
                          'Content-Type': 'application-json'
                        },
                        success: function (res) {
                          console.log('获取信息',res.data);
                          wx.navigateTo({
                            url: '/pages/mainPage/login/main?id=1',
                            success: function(res) {
                              // 通过eventChannel向被打开页面传送数据
                              console.log('跳转页面成功',res);
                            },
                            fail:function(res){
                              console.log('跳转页面失败',res);
                            }
                          })
                         
                          //return;
                          // wxe3786e0f1c17f2f9
                          wx.setStorageSync('information'+context.appid, res.data);
              
                          wx.hideLoading();
                          
              
                          let timestamp = Date.parse(new Date());
                          wx.setStorageSync('timestamp'+context.appid, timestamp);
                          // // 本地存储openId,时效为2小时
                          // wx.setStorageSync('openId', res.data)
                          success();
                        }});
                      
                    }
                  });
                }else{
                  wx.authorize({
                    scope: 'scope.writePhotosAlbum',
                    success(res) {//这里是用户同意授权后的回调
                      console.log( "授权成功"+res);
                      success(res)

                    },
                    fail(res) {//这里是用户拒绝授权后的回调
                      console.log(res);
                      error(res)
                    }
                  })
                }
              }
            })
              
              

            }
            
            
            
            
          },
          fail:function (err) {
            error({
              text : '获取openid失败!',
              res : res.errMsg
            });
            console.log(err);
          }
        })
      }else{
        error({
          text : '登录失败!',
          res : res.errMsg
        })

        console.log('登录失败！' + res.errMsg)
      }

    },
    fail: function (res) {
      error({
        text : 'login登录失败!',
        res : res.errMsg
      })
      console.log('login登录失败', res)
    },
  })
}


export function VeevlinkClient(path,type,data,success,error,text) {
  function veevlinkajax() {
    let list = wx.getStorageSync('information'+context.appid);
    console.log('list',list);
    let url
    if(list.EnableReverseProxy){
      url = list.ReverseProxyUrl + '/services/apexrest' + path;
    }else{
      url = list.InstanceUrl + '/services/apexrest' + path;
    }
    console.log('url',url);
    let header = {
      'Authorization' : 'Bearer '
        +list.SessionId,
      // 'Bearer' : list.vue.SessionId,
      'Content-Type':'application-json'
    };
    wx.showLoading({
      mask : true,
      title: text?text:'加载中',
    });
    console.log('url',url);
    wx.request({
      url: url,
      method:type,
      data: data,
      header: header,
      success:function(res){
        // 查看状态码 是 401 则重新获取session并运行逻辑
        console.log('状态码:'+res.statusCode);
        if(res.statusCode === 401){
          function getSession() {
            wx.request({
              url: list.RefreshTokenProxyUrl+'?SourceId='+list.AppId+'&openid='+list.OpenId,
              method:'GET',
              data: '',
              header: {
                'Content-Type':'application-json'
              },
              success:function(res2){
                list.SessionId = res2.data.access_token;
                console.log(res2.data.access_token);
                wx.setStorageSync('information'+context.appid,list);
                console.log(wx.getStorageSync('information'+context.appid));
                veevlinkajax()
              },
              fail:function (err) {
                getSession();
              }
            })
          }
          getSession();
          return;
        }

        let statusCode = JSON.stringify(res.statusCode);
        // 如果有error为true  就发邮件
        if( res.data.error){
          let datas = {
            Url: path,
            Name:'redstone',
            CreateAt: new Date().toLocaleString(),
            Body:JSON.stringify({
              Url: path,
              Name:'redstone',
              context : JSON.stringify(list),
              body:JSON.stringify(data),
              CreateAt: new Date().toLocaleString(),
              errorMsg:JSON.stringify(res.data)
            })
          };
          wx.request({
            url: 'https://api.veevlink.com/Log/ErrorLog',
            method:'POST',
            data: datas,
            header: {
              'Content-Type':'application-json'
            },
            success:function(res){

            },
            fail:function (err) {
            }
          });
          wx.hideLoading();
          error('网络有点问题,请稍后重试');
          return;
        }
        success(res.data);
        wx.hideLoading();
      },
      fail:function (err) {
        wx.getSystemInfo({
          success: function(res) {
            wx.getNetworkType({
              success: function(res2) {
                // 返回网络类型, 有效值：
                // wifi/2g/3g/4g/unknown(Android下不常见的网络类型)/none(无网络)
                let datas = {
                  Url: path,
                  Name:'8848',
                  CreateAt: new Date().toLocaleString(),
                  Body:JSON.stringify({
                    Url: path,
                    Name:'8848',
                    context : JSON.stringify(list),
                    CreateAt: new Date().toLocaleString(),
                    body:JSON.stringify(data),
                    errorMsg:JSON.stringify(err),
                    getSystemInfo : JSON.stringify(res),
                    getNetworkType : JSON.stringify(res2)
                  })
                };
                wx.request({
                  url: 'https://api.veevlink.com/Log/ErrorLog',
                  method:'POST',
                  data: datas,
                  header: {
                    'Content-Type':'application-json'
                  },
                  success:function(res){

                  },
                  fail:function (err) {
                  }
                });
              }
            });


          }
        });
        //
        if(context.appid === 'wxe3786e0f1c17f2f9'){
          error(err);
        }else {
          error('网络有点问题,请稍后重试');
        }
        wx.hideLoading();
      }
    })
  }
  veevlinkajax();
}

export function UploadClient(path,type,data,success,error) {
  let list = wx.getStorageSync('information'+context.appid);
  console.log(list);
  let url;
  if(list.EnableReverseProxy){
    url = list.ReverseProxyUrl + '/services/apexrest' + path;
  }else{
    url = list.InstanceUrl + '/services/apexrest' + path;
  }

  let header = {
    'Authorization' : 'Bearer '+list.SessionId,
    // 'Bearer' : list.SessionId,
    'Content-Type':'application-json'
  }
  wx.showLoading({
    title: '加载中',
  });
  wx.request({
    url: url,
    method:type,
    data: data,
    header: header,
    success:function(res){
      success(res);
      wx.hideLoading();
    },
    fail:function (err) {
      error(err);
      wx.hideLoading();
    }
  })
}

// VeevlinkData
export function VeevlinkData() {
  return wx.getStorageSync('information'+context.appid);
}
export function formaldata() {
  return formal;
}
// OSS相关域名
export function VeevlinkOssUrl() {
  return context.ossurl;
}
export function VeevlinkOssShowUrl() {
  return context.ossshowurl;
}
//oss测试系统显示图片
// export function testshowurl() {
//
// }

export function Appid() {
  return context.appid;
}

function VeevlinkClientnext(path,type,data,success,error) {
  let list = wx.getStorageSync('information'+context.appid);
  console.log(list);
  let url;
  if(list.EnableReverseProxy){
    url = list.ReverseProxyUrl + '/services/apexrest' + path;
  }else{
    url = list.InstanceUrl + '/services/apexrest' + path;
  }
  let header = {
    'Authorization' : 'Bearer '+list.SessionId,
    // 'Bearer' : list.vue.SessionId,
    'Content-Type':'application-json'
  }
  wx.showLoading({
    title: '加载中',
  });
  wx.request({
    url: url,
    method:type,
    data: data,
    header: header,
    success:function(res){
      success(res.data);
      wx.hideLoading();
    },
    fail:function (err) {
      error(err);
      wx.hideLoading();
    }
  })
}
