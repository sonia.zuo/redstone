/**
 * Created by Luby on 2018/5/31.
 */
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    home:{},
    sign:{},
    member:{},
    actInfo:{},
    add:{},
    addlist:{},
    service:{},
    memberInfo:{},//会员信息-积分商城
    addressSession:{//选择地址-积分商城
      address: "",
      addressId: "",
      city: "",
      district: "",
      memberId: "",
      memberName: "",
      name: "",
      phone: "",
      province: ""
    },
    goodsInfo:{//商品信息-积分商城
        color: "",
        count: 1,
        productId: "",
        exchangeType: "",
        size: null,
        memberId: "",
        image: "",
        exchangePoints: null,
        productName: "",
        total: null
    },
    list:{},
    detail:{}

    
  },
  mutations: {
    add: (state,array) => {
      console.log(array);
      const obj = state;
      obj[array[0]][array[1]] = array[2];
    },
    SAVE_MEMBERINFO:(state,value)=>{
      state.memberInfo = value
    },
    SAVE_ADDRESS_SESSION: (state, value) => {
      state.addressSession = value
    },
    SAVE_GOODSINFO: (state, value) => {
      state.goodsInfo = value
    },
    created: (state)=>{
      for(let name in state){
        // console.log(name);
        state[name] = {}
      }
      console.log(state);
    }
  }
});

export default store
