import Vue from 'vue'
import App from './App'
import store from './store';
Vue.config.productionTip = false
App.mpType = 'app'
Vue.prototype.$store = store
const app = new Vue(App);


// 小程序监控
// require('./utils/tracker.min.js')({token:"f8c084f20a426737d0b380e6b6f15aa4",behaviour:15,origin:[""],exclude:[""]});
// var bugOut = require('./utils/bugOut.min.js')
// bugOut.init(true, '8b3413d21eafab85d9524dceb8a119d5', 'version')
// globalData: { bugOut: bugOut }
// try {
//   //...
// } catch(err) {
//   bugOut.track(err)
// }


var mta= require('./utils/mta_analysis.js');
import { VeevlinkOssUrl,formaldata } from './utils/base'

// 如果OSS是正式的 则 埋点配置也要是正式的
if(VeevlinkOssUrl() === 'https://ebensz-sales.oss-cn-hangzhou.aliyuncs.com'){
  console.log('是正式埋点');
  mta.App.init({
    "appID":"500647903",
    "eventID":"500647904",
    "statPullDownFresh":true,
    "statShareApp":true,
    "statReachBottom":true
  });
}else{
  console.log('是测试埋点');
  mta.App.init(
    {
      "appID":"500630296",
      "eventID":"88481001"
    }
  );
}
new Vue({
  store,
  app
}).$mount()
// app.$mount()


